package casperix.demo

import casperix.app.SafeApplicationAdapter
import casperix.camera.CameraConfig
import casperix.camera.GdxOrthographicCameraController
import casperix.demo.impl.Demo
import casperix.demo.impl.DemoLauncher
import casperix.demo.misc.*
import casperix.demo.misc.DemoConfig.defaultZoom
import casperix.gdx.input.InputCatcher
import casperix.gdx.input.registerInput
import casperix.gdx.renderer.impl.GdxRenderer2D
import casperix.gdx.renderer.impl.MaterialShader
import casperix.input.InputEvent
import casperix.math.color.Colors
import casperix.math.quad_matrix.float32.Matrix3f
import casperix.math.quad_matrix.float32.Matrix4f
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.float32.Vector3f
import casperix.math.vector.int32.Vector2i
import casperix.misc.time.getTimeMs
import casperix.misc.toPrecision
import casperix.renderer.Environment
import casperix.renderer.Renderer2D
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.utils.ScreenUtils
import kotlin.random.Random

class DemoApplication : SafeApplicationAdapter, InputAdapter() {
    val renderer: Renderer2D = GdxRenderer2D()
    val ui = UIController()

    private val camera = CameraProvider.camera
    private val cameraController = GdxOrthographicCameraController(camera, CameraConfig(0.00001f, 1000f))
    private var projection = Matrix4f.IDENTITY
    private var mousePoint = Vector2i.ZERO

    private val random = Random(1)

    private var demoEntry: DemoLauncher? = null
    private var demo: Demo? = null

    init {
        camera.zoom = defaultZoom

        var program = ShaderProgram(
            Gdx.files.classpath("assets/shader/material2d.vertex.glsl").readString(),
            Gdx.files.classpath("assets/shader/material2d.fragment.glsl").readString()
        )

        MaterialShader().program

        registerInput(cameraController)
        registerInput(InputCatcher(::handleInput))
        registerInput(this)


        resize(Gdx.graphics.width, Gdx.graphics.height)

        UIEvents.onDemoLauncher.then {
            demoEntry = it
            val next = it.launcher(random)
            UIEvents.onDemo.set(next)
        }
        UIEvents.onDemo.then {
            demo = it
        }
        UIEvents.onRandomize.then {
            refreshDemo()
        }

        startByPrefix("City 2")
    }

    private fun startByPrefix(prefix: String) {
        UIEvents.onDemoLauncher.set(DemoCollection.items.firstOrNull { it.name.startsWith(prefix, true) } ?: DemoCollection.items.first())
    }

    private fun handleInput(event: InputEvent) {
        demo?.input(event)
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        mousePoint = Vector2i(screenX, screenY)
        return super.mouseMoved(screenX, screenY)
    }


    private fun refreshDemo() {
        val demoEntry = demoEntry ?: return
        UIEvents.onDemoLauncher.set(demoEntry)
    }

    override fun resize(width: Int, height: Int) {
        val wRange = width.toFloat() / 2f * DemoConfig.strangeScale
        val hRange = height.toFloat() / 2f * DemoConfig.strangeScale
        projection = Matrix4f.orthographic(-wRange, wRange, -hRange, hRange, -100f, 100f)
        ui.uiManager.resize(width, height)
    }

    private fun screenToScene(value: Vector2f): Vector2f {
        return camera.transform.unproject(value)

    }

    private fun Matrix4f.customMatrix3f(): Matrix3f {
        return Matrix3f(
            floatArrayOf(
                data[0], data[1], data[3],
                data[4], data[5], data[7],
                data[12], data[13], data[15],
            )
        )
    }

    override fun render() {

        renderer.environment = Environment(camera.transform.view, camera.transform.projection, Colors.PURPLE, Vector3f(500f, 0f, 1000f))

        cameraController.update()

        val scenePoint = screenToScene(mousePoint.toVector2f())
        demo?.update(scenePoint)

        ui.textTitle.setText(demoEntry?.name ?: "<no demo>")
        ui.textInfo.setText(demo?.getDetail() ?: "<no info>")


        ScreenUtils.clear(0.1f, 0.2f, 0.4f, 1f, true)

        demo?.render(renderer)

        renderer.flush()

        ui.drawAndUpdate()

    }

    override fun dispose() {

    }
}