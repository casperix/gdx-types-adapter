package casperix.demo.city2

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.action.SelectorAction
import casperix.demo.impl.action.SimpleAction
import casperix.demo.impl.action.SwitchAction
import casperix.demo.impl.action.TitleEntry
import casperix.demo.impl.shape_builder.SegmentEditor
import casperix.math.mesh.RTreeMeshBuilder
import casperix.math.mesh.float32.Mesh
import casperix.math.random.nextVector2f
import casperix.math.straight_line.float32.LineSegment2f
import casperix.math.vector.float32.Vector2f
import casperix.mesh.MeshInfo
import casperix.mesh.city.City
import casperix.mesh.city.CityMeshGenerator
import casperix.mesh.city.component.CityMeshConfig
import casperix.mesh.city.render.CityMeshRender
import casperix.renderer.Renderer2D
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.random.Random
import kotlin.time.Duration
import kotlin.time.measureTime

@OptIn(ExperimentalUnsignedTypes::class)
class CityDemo2(var random: Random) : AbstractDemo() {
    private var time = Duration.ZERO
    private var cityMesh = Mesh()
    private val cityPivots = mutableListOf<SegmentEditor>()

    private var needRecalculate = true
    private var showRegions = false
    private var showCrosses = false

    private var config = CityMeshConfig(emptyList(), primarySize = 32f)

    init {
        actionManager += TitleEntry("divider")
        actionManager += SelectorAction("div-length", "off") { config = config.copy(divideToLength = null); reset() }
        actionManager += SelectorAction("div-length", "0.5") { config = config.copy(divideToLength = 0.5f); reset() }
        actionManager += SelectorAction("div-length", "1.0") { config = config.copy(divideToLength = 1.0f); reset() }
        actionManager += SelectorAction("div-length", "2.0") { config = config.copy(divideToLength = 2.0f); reset() }

        actionManager += TitleEntry("dispersion angle")
        actionManager += SelectorAction("r-angle", "off") { config = config.copy(roadDegreeDispersion = 0f); reset() }
        actionManager += SelectorAction("r-angle", "5") { config = config.copy(roadDegreeDispersion = 5f); reset() }
        actionManager += SelectorAction("r-angle", "10") { config = config.copy(roadDegreeDispersion = 10f); reset() }
        actionManager += SelectorAction("r-angle", "20", true) { config = config.copy(roadDegreeDispersion = 20f); reset() }
        actionManager += SelectorAction("r-angle", "40") { config = config.copy(roadDegreeDispersion = 40f); reset() }

        actionManager += TitleEntry("dispersion distance")
        actionManager += SelectorAction("dis-value", "off") { config = config.copy(roadDistanceDispersion = 0f); reset() }
        actionManager += SelectorAction("dis-value", "0.1") { config = config.copy(roadDistanceDispersion = 0.1f); reset() }
        actionManager += SelectorAction("dis-value", "0.2") { config = config.copy(roadDistanceDispersion = 0.2f); reset() }
        actionManager += SelectorAction("dis-value", "0.4", true) { config = config.copy(roadDistanceDispersion = 0.4f); reset() }
        actionManager += SelectorAction("dis-value", "0.8") { config = config.copy(roadDistanceDispersion = 0.8f); reset() }

        actionManager += TitleEntry("dispersion chance")
        actionManager += SelectorAction("dis-chance", "off") { config = config.copy(chanceForDispersion = 0f); reset() }
        actionManager += SelectorAction("dis-chance", "20%") { config = config.copy(chanceForDispersion = 0.2f); reset() }
        actionManager += SelectorAction("dis-chance", "50%") { config = config.copy(chanceForDispersion = 0.5f); reset() }
        actionManager += SelectorAction("dis-chance", "80%", true) { config = config.copy(chanceForDispersion = 0.8f); reset() }

        actionManager += TitleEntry("cities")
        actionManager += SimpleAction("add") {
            addCity()
        }
        actionManager += SimpleAction("remove") {
            cityPivots.removeLastOrNull()?.dispose()
            reset()
        }
        actionManager += TitleEntry("misc")
        actionManager += SwitchAction("intercity roads") { config = config.copy(intercityRoads = it); reset() }
        actionManager += SwitchAction("primary roads") { config = config.copy(primaryRoads = it); reset() }
        actionManager += SwitchAction("secondary roads") { config = config.copy(secondaryRoads = it); reset() }
        actionManager += SwitchAction("post remove edges") { config = config.copy(postRemoveEdge = it); reset() }
        actionManager += TitleEntry("view")
        actionManager += SwitchAction("show regions") {
            showRegions = it
        }
        actionManager += SwitchAction("show cross") {
            showCrosses = it
        }
        pointManager.pointDragged.then {
            reset()
        }

        cityPivots += SegmentEditor(pointManager, LineSegment2f.byDelta(Vector2f(-20f, 10f), Vector2f(20f)))
        cityPivots += SegmentEditor(pointManager, LineSegment2f.byDelta(Vector2f(20f, -10f), Vector2f(20f)))
        reset()
    }

    private fun addCity() {
        val start = random.nextVector2f(-10f, 10f)
        val delta = random.nextVector2f(-3f, 3f)
        cityPivots += SegmentEditor(pointManager, LineSegment2f.byDelta(start, delta))
        reset()
    }

    private fun reset() {
        needRecalculate = true
    }

    override fun getArticles(): List<String> {
        return MeshInfo.get(cityMesh) + listOf(
            "time: " + time.inWholeMilliseconds + "ms",
        )
    }

    override fun update(customPoint: Vector2f) {
        if (!needRecalculate) {
            return
        }
        needRecalculate = false

        random = Random(1)
        time = measureTime {
            cityMesh = generateCityMesh()
        }
    }

    override fun renderScene(renderer: Renderer2D) {
        CityMeshRender.render(renderer, cityMesh, showCrosses, true, showRegions)
    }

    private fun generateCityMesh(): Mesh {
        val cityList = cityPivots.mapIndexed { index, segment ->
            City(Random(index), segment.line.start, (segment.line.length() * 0.1f).pow(2).roundToInt())
        }
        config = config.copy(cityList = cityList)
        return CityMeshGenerator(RTreeMeshBuilder(), config).build()
    }
}

