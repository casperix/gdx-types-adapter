package casperix.demo.city_region1

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.action.SimpleAction
import casperix.demo.impl.shape_builder.QuadEditor
import casperix.demo.impl.shape_builder.TriangleEditor
import casperix.demo.util.RenderConfig
import casperix.math.collection.getLooped
import casperix.math.geometry.Polygon2f
import casperix.math.geometry.Triangle
import casperix.math.geometry.fPI2
import casperix.math.polar.float32.PolarCoordinateFloat
import casperix.math.random.nextFloat
import casperix.math.random.nextQuad2f
import casperix.math.random.nextRadianFloat
import casperix.math.random.nextTriangle2f
import casperix.math.vector.float32.Vector2f
import casperix.misc.Disposable
import casperix.renderer.Renderer2D
import kotlin.random.Random

class CityRegionDemo1(val random: Random) : AbstractDemo() {
    private val regionEditors = mutableListOf<Any>()
    private var regions = listOf<Polygon2f>()

    init {
        actionManager +=  SimpleAction("add triangle region") {
            addTriangleRegion()
        }
        actionManager +=  SimpleAction("add quad region") {
            addQuadRegion()

        }
        actionManager +=  SimpleAction("remove last region") {
            removeLastRegion()
        }
        actionManager +=  SimpleAction("create radial city") {
            createRadialCity()
        }

        createRadialCity()

        updateRegions()
        pointManager.pointDragged.then {
            updateRegions()
        }
    }

    private fun createRadialCity() {
        clearAllRegions()

        val basis = listOf(
            PolarCoordinateFloat(random.nextFloat(6f, 12f), random.nextRadianFloat(fPI2 * 0.05f, fPI2 * 0.15f)).toDecart(),
            PolarCoordinateFloat(random.nextFloat(6f, 12f), random.nextRadianFloat(fPI2 * 0.20f, fPI2 * 0.30f)).toDecart(),
            PolarCoordinateFloat(random.nextFloat(6f, 12f), random.nextRadianFloat(fPI2 * 0.35f, fPI2 * 0.45f)).toDecart(),
            PolarCoordinateFloat(random.nextFloat(6f, 12f), random.nextRadianFloat(fPI2 * 0.50f, fPI2 * 0.60f)).toDecart(),
            PolarCoordinateFloat(random.nextFloat(6f, 12f), random.nextRadianFloat(fPI2 * 0.65f, fPI2 * 0.75f)).toDecart(),
            PolarCoordinateFloat(random.nextFloat(6f, 12f), random.nextRadianFloat(fPI2 * 0.80f, fPI2 * 0.90f)).toDecart(),
            PolarCoordinateFloat(random.nextFloat(6f, 12f), random.nextRadianFloat(fPI2 * 0.95f, fPI2 * 1.00f)).toDecart(),
        )

        basis.indices.forEach {
            regionEditors += TriangleEditor(
                pointManager,
                Triangle(
                    basis[it],
                    Vector2f.ZERO,
                    basis.getLooped(it + 1),
                )
            )
        }

        updateRegions()
    }


    private fun clearAllRegions() {
        regionEditors.forEach {
            if (it is Disposable) {
                it.dispose()
            }
        }
        regionEditors.clear()
        updateRegions()
    }

    private fun removeLastRegion() {
        val editor = regionEditors.removeLastOrNull() ?: return
        if (editor is Disposable) {
            editor.dispose()
        }
        updateRegions()
    }

    private fun addTriangleRegion() {
        regionEditors += TriangleEditor(pointManager, random.nextTriangle2f(Vector2f(-3f), Vector2f.ZERO))
        updateRegions()
    }

    private fun addQuadRegion() {
        regionEditors += QuadEditor(pointManager, random.nextQuad2f(Vector2f.ZERO, Vector2f(5f)))
        updateRegions()
    }

    private fun updateRegions() {
        val random = Random(1)

        regions = regionEditors.flatMap {
            if (it is TriangleEditor) {
                RegionDivider.divide(it.triangle, random)
            } else if (it is QuadEditor) {
                RegionDivider.divide(it.quad, random)
            } else {
                emptyList()
            }
        }
    }

    override fun getArticles(): List<String> {
        return listOf(
            "amount: ${regionEditors.size}",
        )
    }

    override fun update(customPoint: Vector2f) {

    }

    override fun renderScene(renderer: Renderer2D) {
        regions.forEach {
            renderer.drawPolygonWithContour(RenderConfig.SHAPE_PRIMARY, it)
        }
    }

}

