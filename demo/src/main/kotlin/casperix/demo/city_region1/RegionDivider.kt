package casperix.demo.city_region1

import casperix.demo.util.firstToEnd
import casperix.math.angle.float32.DegreeFloat
import casperix.math.collection.getLooped
import casperix.math.geometry.*
import casperix.math.geometry.float32.Geometry2Float
import casperix.math.geometry.float32.isConvex
import casperix.math.interpolation.interpolateOf
import casperix.math.vector.float32.Vector2f
import kotlin.math.absoluteValue
import kotlin.math.max
import kotlin.math.min
import kotlin.random.Random

object RegionDivider {
    data class RegionDividerContext(val random: Random) {
        val result = mutableListOf<Polygon2f>()
        var overflowControl = 0
    }

    val minDivideLength = 2f

    fun divide(shape: Polygon2f, random: Random): List<Polygon2f> {
        val context = RegionDividerContext(random)
        divide(shape, context)
        return context.result
    }

    private fun divide(shape: Polygon2f, context: RegionDividerContext) {
        if (shape is Triangle2f) {
            divideTriangle(shape, context)
        } else if (shape is Quad2f) {
            divideQuad(shape, context)
        } else {
            throw Exception("Unsupported shape: $shape")
        }
    }

    private fun divideTriangle(triangle: Triangle2f, context: RegionDividerContext) {
        val e01 = triangle.getEdge(0)//v0:v1
        val e12 = triangle.getEdge(1)//v1:v2
        val e20 = triangle.getEdge(2)//v2:v0

        val l01 = e01.length()
        val l12 = e12.length()
        val l20 = e20.length()

        val lengthList = listOf(l01, l12, l20).sorted()
        val minLength = lengthList[0]
        val medianLength = lengthList[1]


        if (medianLength > minDivideLength) {
            triangle.apply {
                val subShapes = if (minLength == l20) {
                    divideTriangleBy2(Triangle(v0, v1, v2))
                } else if (minLength == l01) {
                    divideTriangleBy2(Triangle(v1, v2, v0))
                } else if (minLength == l12) {
                    divideTriangleBy2(Triangle(v1, v0, v2))
                } else {
                    throw Exception("Invalid length")
                }

                subShapes.forEach {
                    divide(it, context)
                }

            }
        } else {
            context.result += triangle
        }
    }


    class SplitCandidate(val index: Int, val dist: Float, val quad: Quad2f, val triangle: Triangle2f) {
        val score: Float = getTriangleScore(triangle) * getQuadScore(quad)
    }

    private fun normalizeQuad(quad: Quad2f): List<Polygon2f> {
        val edges = quad.getEdgeList()

        var quadChecked = quad

        val candidateList = mutableListOf<SplitCandidate>()

        (0 until 3).forEach { offset ->
            val target = edges.getLooped(2 + offset)
            val proj0 = Geometry2Float.projectionBySegment(quadChecked.v0, target)
            if (proj0 != null) {
                val distOther = quadChecked.v2.distTo(proj0)
                val distMain = quadChecked.v3.distTo(proj0)
                if (distMain >= 1f && distOther >= 1f) {
                    candidateList += SplitCandidate(
                        offset,
                        distMain,
                        Quad2f(quadChecked.v0, quadChecked.v1, quadChecked.v2, proj0),
                        Triangle2f(quadChecked.v0, proj0, quadChecked.v3)
                    )
                }
            }
            val proj1 = Geometry2Float.projectionBySegment(quadChecked.v1, target)
            if (proj1 != null) {
                val distOther = quadChecked.v3.distTo(proj1)
                val distMain = quadChecked.v2.distTo(proj1)
                if (distMain >= 1f && distOther >= 1f) {
                    candidateList += SplitCandidate(
                        offset,
                        distMain,
                        Quad2f(quadChecked.v0, quadChecked.v1, proj1, quadChecked.v3),
                        Triangle2f(quadChecked.v1, quadChecked.v2, proj1)
                    )
                }
            }

            quadChecked = quadChecked.firstToEnd()
        }

        val candidate = candidateList.filter { it.score > 0f }.sortedBy { it.score }.lastOrNull()
            ?: return emptyList()


        return listOf(candidate.quad, candidate.triangle)
    }

    private fun getTriangleScore(triangle: Triangle2f): Float {

        val lengthList = triangle.getEdgeList().map {
            it.length()
        }
        val lengthMax = lengthList.max()
        val lengthMin = lengthList.min()

        val factor = lengthMin / lengthMax
        if (factor < 0.5f) return 0f
        return factor
    }

    private fun getQuadScore(quad: Quad2f): Float {
        var score = 0f

        val edges = quad.getEdgeList()
        edges.indices.forEach {
            val a = edges.getLooped(it).delta().normalize()
            val b = -edges.getLooped(it - 1).delta().normalize()

            val angle = DegreeFloat.betweenAngles(DegreeFloat.byDirection(a), DegreeFloat.byDirection(b)).value.absoluteValue

            if (angle > 85f) {
                score += 1f
            } else if (angle > 75f) {
                score += 0.2f
            }
        }
        return score
    }


    private fun divideQuad(quad: Quad2f, context: RegionDividerContext) {
        if (!quad.isConvex()) {
            return
        }

        if (context.overflowControl++ > 10000) {
            return
        }

//        val subShapes = normalizeQuad(quad)
//        if (subShapes.isNotEmpty()) {
//            subShapes.forEach {
//                divide(it, context)
//            }
//            return
//        }

        val e0 = quad.getEdge(0)
        val e1 = quad.getEdge(1)
        val e2 = quad.getEdge(2)
        val e3 = quad.getEdge(3)

        val u = min(e0.length(), e2.length())
        val v = min(e1.length(), e3.length())

        val maxEdge = max(u, v)

        if (maxEdge >= minDivideLength) {
            val factor = 0.5f//random.nextFloat(0.4f, 0.6f)
            val subRegions = if (maxEdge == v) {
                splitByUAxis(quad, factor)
            } else {
                splitByVAxis(quad, factor)
            }
            subRegions.forEach {
                divideQuad(it, context)
            }
        } else {
            context.result += quad
        }
    }

    /**
     * v1
     * |
     * |
     * m1--m2
     * |
     * |
     * v0------v2
     */
    private fun divideTriangleBy2(triangle: Triangle2f): List<Polygon2f> {
        triangle.run {
            val m1 = (v0 + v1) / 2f
            val m2 = (v1 + v2) / 2f

            return listOf(Quad2f(v0, v2, m2, m1), Triangle2f(m1, m2, v1))
        }
    }


    /**
     * v3---v2
     * |
     * m1---m2-->U
     * |
     * v0---v1
     */
    private fun splitByUAxis(candidate: Quad2f, factor: Float): List<Quad2f> {
        val m1 = interpolateOf(candidate.v0, candidate.v3, factor)
        val m2 = interpolateOf(candidate.v1, candidate.v2, factor)

        candidate.run {

            return listOf(
                Quad2f(
                    v0, v1,
                    m2, m1
                ),
                Quad2f(
                    m1, m2,
                    v2, v3
                ),
            )
        }
    }

    private fun splitByVAxis(candidate: Quad2f, factor: Float): List<Quad2f> {
        candidate.apply {
            return splitByUAxis(Quad2f(v1, v2, v3, v0), factor)
        }
    }

    private fun splitByU2(candidate: Quad2f, m2: Vector2f): List<Quad2f> {
        val m1 = (candidate.v0 + candidate.v3) / 2f

        return listOf(
            Quad2f(
                candidate.v0, candidate.v1,
                m2, m1
            ),
            Quad2f(
                m1, m2,
                candidate.v2, candidate.v3
            )
        )
    }
}

