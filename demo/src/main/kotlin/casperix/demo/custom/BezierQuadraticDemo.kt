package casperix.demo.custom

import casperix.demo.impl.shape_builder.BezierQuadraticBuilder
import casperix.demo.impl.AbstractDemo
import casperix.demo.util.RenderConfig.SHAPE_SECONDARY
import casperix.math.vector.float32.Vector2f
import casperix.misc.toPrecision
import casperix.renderer.Renderer2D
import kotlin.random.Random


class BezierQuadraticDemo(val random: Random) : AbstractDemo() {
    private val bezierBuilder = BezierQuadraticBuilder(random)
    private var bezier = bezierBuilder.build()

    init {
        pointManager.groups += bezierBuilder.getPoints()
    }

    override fun getDetail(): String {
        return listOf(
            "length:" + (bezier.length()).toPrecision(1),
            "p0:" + (bezier.p0).toPrecision(1),
            "p1:" + (bezier.p1).toPrecision(1),
            "p2:" + (bezier.p2).toPrecision(1),
        ).joinToString("\n")
    }

    override fun update(customPoint: Vector2f) {
        bezier = bezierBuilder.build()
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.drawCurve( bezier)
        renderer.drawCurvePoints( bezier)

        renderer.drawPoint(SHAPE_SECONDARY, bezier.p0)
        renderer.drawPoint(SHAPE_SECONDARY, bezier.p1)
        renderer.drawPoint(SHAPE_SECONDARY, bezier.p2)
    }



}

