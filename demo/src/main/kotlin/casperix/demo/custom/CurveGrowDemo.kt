package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.shape_builder.*
import casperix.demo.util.RenderConfig.SHAPE_SECONDARY
import casperix.demo.util.RenderConfig.SHAPE_PRIMARY
import casperix.demo.util.RenderConfig.lineThick
import casperix.math.curve.float32.Curve2f
import casperix.math.curve.float32.LineCurve2f
import casperix.math.geometry.delta
import casperix.math.geometry.float32.Geometry2Float
import casperix.math.intersection.float32.Intersection2Float
import casperix.math.random.nextArc2f
import casperix.math.random.nextLine2f
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D

import kotlin.random.Random


class CurveGrowDemo(val random: Random) : AbstractDemo() {
    class Entry(val builder: ShapeEditor, val p1: ControlPoint, val p2: ControlPoint)

    private val entries = listOf(
        Entry(LineEditor(pointManager, random.nextLine2f()), ControlPoint(Vector2f.ZERO), ControlPoint(Vector2f.ZERO)),
        Entry(ArcEditor(pointManager, random.nextArc2f()), ControlPoint(Vector2f.ZERO), ControlPoint(Vector2f.ZERO)),
    )

    init {
        entries.forEach {
            pointManager.groups += (listOf(it.p1) + it.builder.getPoints() + listOf(it.p2))
        }
    }

    override fun getDetail(): String {
        return ""
    }

    override fun update(customPoint: Vector2f) {
    }

    override fun renderScene(renderer: Renderer2D) {
        val curveList = entries.map {
            it.builder.shape
        }

        val curveListGrow = entries.mapIndexed { index, it ->
            val curve = curveList[index] as Curve2f
            if (curve is LineCurve2f) {
                val line = curve.line
                val grow1 = Geometry2Float.projectionByDirectionLength(it.p1.position - line.v0, -line.delta())
                val grow2 = Geometry2Float.projectionByDirectionLength(it.p2.position - line.v1, line.delta())
                curve.grow(grow1, grow2)
            } else {
                val delta = curve.finish - curve.start
                val grow1 = Geometry2Float.projectionByDirectionLength(it.p1.position - curve.start, -delta)
                val grow2 = Geometry2Float.projectionByDirectionLength(it.p2.position - curve.finish, delta)
                curve.grow(grow1, grow2)
            }
        }


        curveList.forEach {
            renderer.drawCurve(SHAPE_PRIMARY, it as Curve2f, lineThick * 2f, 1000)
        }

        curveListGrow.forEach {
            renderer.drawCurve(SHAPE_SECONDARY, it, lineThick, 1000)
        }

        val intersectionList = Intersection2Float.getCurveWithCurve(curveListGrow.first(), curveListGrow.last())
        intersectionList.forEach {
            renderer.drawPoint(SHAPE_SECONDARY, it.position)
        }
    }
}

