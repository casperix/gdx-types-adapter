package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.misc.CameraProvider
import casperix.math.color.ColorCode
import casperix.math.color.Colors
import casperix.math.curve.float32.BezierQuadratic2f
import casperix.math.curve.float32.Curve2f
import casperix.math.curve.float32.LineCurve2f
import casperix.math.curve.float32.UniformCurve2f
import casperix.math.geometry.Line2f
import casperix.math.polar.float32.PolarCoordinateFloat
import casperix.math.random.nextFloat
import casperix.math.random.nextRadianFloat
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D


import kotlin.random.Random


class CurveUnionDemo(val random: Random) : AbstractDemo() {

    val union: Curve2f
    val scale = 10f
    val parts = 10

    init {

        val points = generatePoints(5)

        val a: Curve2f = UniformCurve2f(BezierQuadratic2f(points[0], points[1], points[2]), parts)
        val b: Curve2f = LineCurve2f(Line2f(points[2], points[3]))
        val c: Curve2f = LineCurve2f(Line2f(points[3], points[4]))

        union = a + b + c
    }

    private fun generatePoints(amount: Int): List<Vector2f> {
        var last = Vector2f.ZERO
        return (0 until amount).map {
            val current = last

            val angle = random.nextRadianFloat()
            val range = random.nextFloat(0.2f, 1f) * scale
            last += PolarCoordinateFloat(range, angle).toDecart()

            current
        }
    }

    override fun getDetail(): String {

        return ""
    }

    override fun update(customPoint: Vector2f) {

    }

    override fun renderScene(renderer: Renderer2D) {
        val lineThick = CameraProvider.camera.zoom * 2f
        val pointRange = CameraProvider.camera.zoom * 6f

        renderer.drawCurve(Colors.GREEN, union, lineThick)

        repeat(parts + 1) {
            val p = union.getPosition(it / parts.toFloat())
            renderer.drawPoint(Colors.GREEN, p, pointRange)
        }

    }
}

