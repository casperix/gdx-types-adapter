package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.math.color.Colors
import casperix.math.geometry.float32.Geometry2Float.distPointToSegment
import casperix.math.random.nextLine2f
import casperix.math.vector.float32.Vector2f
import casperix.misc.toPrecision
import casperix.renderer.Renderer2D
import kotlin.random.Random

class DestToSegmentDemo(val random: Random) : AbstractDemo() {

    val line = random.nextLine2f()

    var customPoint = Vector2f.ZERO

    override fun update(customPoint: Vector2f) {
        this.customPoint = customPoint
    }

    override fun getDetail(): String {
        val dest = distPointToSegment(customPoint, line)
        return "Dest:" + dest.toPrecision(6)
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.drawLine(Colors.GREEN, line)
        renderer.drawPoint(Colors.RED, customPoint)

    }
}