package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.shape_builder.LineBuilder
import casperix.demo.util.RenderConfig.SHAPE_PRIMARY
import casperix.demo.util.RenderConfig.SHAPE_MARKER
import casperix.math.geometry.Line2f
import casperix.math.geometry.float32.Geometry2Float.closestConnectionBetweenSegments
import casperix.math.geometry.float32.Geometry2Float.closestSegmentPoint
import casperix.math.geometry.length
import casperix.math.vector.float32.Vector2f
import casperix.misc.toPrecision
import casperix.renderer.Renderer2D
import kotlin.random.Random

class LineWithLineClosestDemo(val random: Random) : AbstractDemo() {

    val lineA = LineBuilder(random)
    var lineB = LineBuilder(random)

    val O = ControlPoint(Vector2f.ONE)

    var closestPoint: Vector2f? = null
    var closestConnection: Line2f? = null

    init {
        listOf(lineA, lineB).flatMap {
            it.getPoints()
        }

        pointManager.groups += listOf(O)

        register(lineA)
        register(lineB)
    }


    override fun update(customPoint: Vector2f) {
        val lineA = lineA.build().line
        val lineB = lineB.build().line

        closestPoint = closestSegmentPoint(lineA, O.position)
        closestConnection = closestConnectionBetweenSegments(lineA, lineB)

    }

    override fun getArticles(): List<String> {
        val intersection = closestConnection

        return listOf(
            "intersection: " + (if (intersection != null) intersection.length().toPrecision(2) else "<no>"),
        )
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.drawLine(SHAPE_PRIMARY, lineA.build().line)
        renderer.drawLine(SHAPE_PRIMARY, lineB.build().line)

        closestConnection?.let {
            renderer.drawLine(SHAPE_MARKER, it)
        }
        closestPoint?.let {
            renderer.drawPoint(SHAPE_MARKER, it)
        }
    }

}