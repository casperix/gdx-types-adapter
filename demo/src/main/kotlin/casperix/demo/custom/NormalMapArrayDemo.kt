package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.control_point.ControlPoint
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D
import kotlin.random.Random

class NormalMapArrayDemo(val random: Random) : AbstractDemo() {
    val lightPosition = ControlPoint(Vector2f.ONE)

    init {
        pointManager.groups += listOf(lightPosition)
    }

    override fun update(customPoint: Vector2f) {
    }

    override fun getDetail(): String {
        return ""
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.environment = renderer.environment.copy(lightPosition = lightPosition.position.expand(1f))
//        renderer.drawGraphic(VectorGraphicProvider.texturedTangentGraphic)
    }

}


