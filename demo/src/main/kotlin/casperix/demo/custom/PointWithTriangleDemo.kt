package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.math.color.Colors
import casperix.math.intersection.float32.Intersection2Float
import casperix.math.random.nextTriangle2f
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D
import kotlin.random.Random

class PointWithTriangleDemo(val random: Random) : AbstractDemo() {

    val triangle = random.nextTriangle2f(-10f, 10f)
    var point = Vector2f.ZERO
    var hasIntersection = false


    override fun update(customPoint: Vector2f) {
        point = customPoint

        hasIntersection = Intersection2Float.hasPointWithTriangle(point, triangle)

    }

    override fun getDetail(): String {
        return "Intersection: $hasIntersection"
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.drawTriangle(Colors.GREEN, triangle)

        val pointColor = if (hasIntersection) {
            Colors.RED
        } else {
            Colors.BLUE
        }
        renderer.drawPoint(pointColor, point)
    }
}