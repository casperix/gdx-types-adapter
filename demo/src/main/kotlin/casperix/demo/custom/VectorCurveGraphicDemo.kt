package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.shape_builder.ArcEditor
import casperix.demo.misc.Assets
import casperix.math.angle.float32.RadianFloat
import casperix.math.axis_aligned.float32.Box2f
import casperix.math.collection.getLooped
import casperix.math.curve.float32.Arc2f
import casperix.math.curve.float32.Curve2f
import casperix.math.geometry.builder.Quadulator
import casperix.math.random.nextArc2f
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.toQuad
import casperix.renderer.Renderer2D
import casperix.renderer.vector.VectorGraphic
import casperix.renderer.vector.builder.VectorBuilder
import kotlin.random.Random

@ExperimentalUnsignedTypes
class VectorCurveGraphicDemo(val random: Random) : AbstractDemo() {
    val arcEditor = ArcEditor(pointManager, random.nextArc2f())
    var arc: Curve2f = Arc2f(Vector2f.ONE, RadianFloat.ZERO, RadianFloat.ZERO, 1f)
    val Light = ControlPoint(Vector2f(2f, 2f))

    var graphic = VectorGraphic.EMPTY

    init {
        pointManager.groups += arcEditor.getPoints()
        pointManager.groups += listOf(Light)
    }

    override fun getDetail(): String {
        return ""
    }

    override fun update(customPoint: Vector2f) {
        val next = arcEditor.shape
        if (arc != next) {
            arc = next
            rebuildGraphic()
        }

    }

    private fun rebuildGraphic() {
        graphic = VectorBuilder(hasPosition2 = true, hasTextureCoord = true, hasTangent = true).buildGraphic(Assets.road) {
            val parts = 100
            val uScale = 0.2f * arcEditor.shape.length() / parts.toFloat()
            Quadulator.curve(arcEditor.shape, 1f, parts).forEachIndexed { index, quad ->
                val u1 = index.toFloat() * uScale
                val u2 = (index + 1).toFloat() * uScale
                val tex = Box2f(Vector2f(u1, 0.75f), Vector2f(u2, 0.85f)).toQuad().getVertices()
                val vertices = quad.getVertices()
                val tangent = (vertices.getLooped(1) - vertices[0]).normalize()
                addQuad {
                    setPosition2(vertices[it])
                    setTextureCoord(tex[it])
                    setTangent(tangent)
                }
            }
        }
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.environment = renderer.environment.copy(lightPosition = Light.position.expand(1f))

        renderer.drawGraphic(graphic)
    }
}



