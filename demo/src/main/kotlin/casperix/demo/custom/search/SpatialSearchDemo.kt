package casperix.demo.custom.search

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.action.SelectorAction
import casperix.demo.impl.action.SimpleAction
import casperix.demo.impl.action.TitleEntry
import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.util.RenderConfig.SHAPE_SECONDARY
import casperix.demo.util.RenderConfig.lineThick
import casperix.math.axis_aligned.float32.Box2f
import casperix.math.axis_aligned.float32.getAABBox
import casperix.math.color.Color
import casperix.math.color.Colors
import casperix.math.curve.float32.Circle2f
import casperix.math.geometry.Quad2f
import casperix.math.geometry.builder.Triangulator
import casperix.math.geometry.float32.isConvex
import casperix.math.mesh.SpatialMap
import casperix.math.mesh.component.RTreeEdge
import casperix.math.mesh.component.RTreePoint
import casperix.math.mesh.component.RTreeRegion
import casperix.math.mesh.float32.MeshEdge
import casperix.math.mesh.float32.MeshPoint
import casperix.math.mesh.float32.MeshRegion
import casperix.math.random.nextPolarCoordinateFloat
import casperix.math.random.nextVector2f
import casperix.math.straight_line.float32.LineSegment2f
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.toQuad
import casperix.misc.clamp
import casperix.misc.toPrecision
import casperix.renderer.Renderer2D
import casperix.renderer.material.Material
import casperix.renderer.vector.VectorGraphic
import casperix.renderer.vector.vertex.Vertex
import casperix.renderer.vector.vertex.VertexColor
import casperix.renderer.vector.vertex.VertexPosition2
import casperix.renderer.vector.vertex.mapTriangles
import com.badlogic.gdx.Gdx
import kotlin.math.roundToInt
import kotlin.random.Random
import kotlin.time.Duration
import kotlin.time.measureTime


@ExperimentalUnsignedTypes
class SpatialSearchDemo(val random: Random) : AbstractDemo() {
    enum class SelectorMode {
        CIRCLE,
        EDGE,
        AABBOX,
        QUAD,
    }

    enum class ElementType {
        POINT,
        EDGE,
        REGION,
    }

    private val VIEW_BASE_SIZE = 0.05f
    private val BASE_AMOUNT = 1_000
    private val SIZE_DISPERSION = 0.50f
    private val SCENE_DIMENSION = Vector2f(20f)

    private val DEFAULT_POINT_COLOR = Colors.GRAY
    private val SELECTED_POINT_COLOR = Colors.WHITE
    private val AABBOX_COLOR = Colors.BLACK.setAlpha(0.2f)

    private val pivotA = ControlPoint(Vector2f(1f, 2f))
    private val pivotB = ControlPoint(Vector2f(4f, 8f))
    private val pivotC = ControlPoint(Vector2f(4f, 12f))
    private val pivotD = ControlPoint(Vector2f(0f, 12f))

    private var elementMap: SpatialMap<*> = RTreePoint()
    private var elementMode = ElementType.REGION
    private var selectorMode = SelectorMode.AABBOX


    private var selectedElements: Collection<Any> = listOf()
    private var searchTime = Duration.ZERO
    private var graphic = VectorGraphic.EMPTY

    init {
        pointManager.groups += listOf(pivotA, pivotB, pivotC, pivotD)
        actionManager.entries += listOf(
            TitleEntry("selector type"),
            SelectorAction("selector", "quad") { setSelectorMode(SelectorMode.QUAD) },
            SelectorAction("selector", "aabbox") { setSelectorMode(SelectorMode.AABBOX) },
            SelectorAction("selector", "edge") { setSelectorMode(SelectorMode.EDGE) },
            SelectorAction("selector", "circle") { setSelectorMode(SelectorMode.CIRCLE) },

            TitleEntry("element type"),
            SelectorAction("generated", "edge") { setElementType(ElementType.EDGE) },
            SelectorAction("generated", "region") { setElementType(ElementType.REGION) },
            SelectorAction("generated", "point") { setElementType(ElementType.POINT) },

            TitleEntry("amount control"),
            SimpleAction("add some") { addSome() },
            SimpleAction("remove some") { removeSome() },
        )

        addAmount(BASE_AMOUNT)

        pointManager.pointDragged.then {
            rebuildSelection()
            rebuildGraphic()
        }
    }

    private fun createRandomPoint(): MeshPoint {
        return MeshPoint(random.nextPolarCoordinateFloat(0f, SCENE_DIMENSION.x).toDecart())
    }

    private fun createRandomEdge(): MeshEdge {
        val start = random.nextPolarCoordinateFloat(0f, SCENE_DIMENSION.x).toDecart()
        val finish = start + random.nextVector2f(-SIZE_DISPERSION, SIZE_DISPERSION)
        return MeshEdge(start, finish)
    }

    private fun createRandomRegion(): MeshRegion {
        while (true) {
            val start = random.nextPolarCoordinateFloat(0f, SCENE_DIMENSION.x).toDecart()
            val points = (0..3).map { start + random.nextVector2f(-SIZE_DISPERSION, SIZE_DISPERSION) }
            val q = Quad2f.from(points)!!
            if (q.isConvex()) {
                return MeshRegion(q)
            }
        }
    }

    private fun addRandomElement() {
        val map = elementMap
        when (map) {
            is RTreePoint -> map.add(createRandomPoint())
            is RTreeEdge -> map.add(createRandomEdge())
            is RTreeRegion -> map.add(createRandomRegion())
        }
    }

    private fun removeLastElement() {
        val map = elementMap
        val items = map.all()
        if (items.isEmpty()) return
        val last = items.last()

        when (map) {
            is RTreePoint -> map.remove(last as MeshPoint)
            is RTreeEdge -> map.remove(last as MeshEdge)
            is RTreeRegion -> map.remove(last as MeshRegion)
        }
    }

    private fun getElementAmount(): Int {
        return elementMap.all().size
    }

    private fun addSome() {
        val some = (getElementAmount() * 1.1f).roundToInt().clamp(100, 5_000)
        repeat(some) {
            addRandomElement()
        }
        rebuildAll()
    }

    private fun removeSome() {
        val some = (getElementAmount() * 0.1f).roundToInt().clamp(100, 5_000)
        repeat(some) {
            removeLastElement()
        }
        rebuildAll()
    }

    private fun setSelectorMode(value: SelectorMode) {
        selectorMode = value
        rebuildAll()
    }

    private fun setElementType(value: ElementType) {
        if (elementMode == value) return
        elementMode = value

        val lastAmount = getElementAmount()

        elementMap = when (value) {
            ElementType.REGION -> RTreeRegion()
            ElementType.EDGE -> RTreeEdge()
            ElementType.POINT -> RTreePoint()
        }

        addAmount(lastAmount)

    }

    private fun addAmount(amount: Int) {
        repeat(amount) {
            addRandomElement()
        }

        rebuildAll()
    }


    override fun getDetail(): String {

        return listOf(
            "fps: " + Gdx.graphics.framesPerSecond,
            "elements: " + getElementAmount(),
            "selected: " + selectedElements.size,
            "time: " + (searchTime.inWholeMicroseconds.toDouble() / 1000.0).toPrecision(3) + "ms",
        ).joinToString("\n")
    }

    override fun update(customPoint: Vector2f) {

    }

    private fun asBoxSelection(): Box2f {
        return Box2f.byCorners(pivotA.position, pivotB.position)
    }

    private fun asQuadSelection(): Quad2f {
        return Quad2f(pivotA.position, pivotB.position, pivotC.position, pivotD.position)
    }

    private fun asEdgeSelection(): LineSegment2f {
        return LineSegment2f(pivotA.position, pivotB.position)
    }

    private fun asCircleSelection(): Circle2f {
        return Circle2f(pivotA.position, pivotB.position.distTo(pivotA.position))
    }

    private fun rebuildAll() {
        rebuildSelection()
        rebuildGraphic()
    }

    private fun rebuildSelection() {
        searchTime = measureTime {
            selectedElements = when (selectorMode) {
                SelectorMode.QUAD -> elementMap.search(asQuadSelection())
                SelectorMode.AABBOX -> elementMap.search(asBoxSelection())
                SelectorMode.EDGE -> elementMap.search(asEdgeSelection())
                SelectorMode.CIRCLE -> elementMap.search(asCircleSelection())
            }
        }
    }

    private fun getColor(element: Any): Color {
        val selected = selectedElements.contains(element)
        return if (selected) SELECTED_POINT_COLOR else DEFAULT_POINT_COLOR

    }

    private fun rebuildGraphic() {
        val map = elementMap
        val triangles = when (map) {
            is RTreeRegion -> {
                map.all().flatMap { region ->
                    Triangulator.quad(region.shape.getAABBox().toQuad()).mapTriangles {
                        Vertex(VertexPosition2(it), VertexColor(AABBOX_COLOR))
                    }
                } + map.all().flatMap { region ->
                    val color = getColor(region)

                    Triangulator.quad(region.shape as Quad2f).mapTriangles {
                        Vertex(VertexPosition2(it), VertexColor(color))
                    }
                }
            }

            is RTreeEdge -> {
                map.all().flatMap { edge ->
                    Triangulator.quad(edge.shape.getAABBox().toQuad()).mapTriangles {
                        Vertex(VertexPosition2(it), VertexColor(AABBOX_COLOR))
                    }
                } + map.all().flatMap { edge ->
                    val color = getColor(edge)

                    Triangulator.segment(edge.shape, VIEW_BASE_SIZE).mapTriangles {
                        Vertex(VertexPosition2(it), VertexColor(color))
                    }
                }
            }


            is RTreePoint -> {
                map.all().flatMap { point ->
                    val color = getColor(point)

                    Triangulator.point(point.shape, VIEW_BASE_SIZE, 8).mapTriangles {
                        Vertex(VertexPosition2(it), VertexColor(color))
                    }
                }
            }

            else -> {
                throw Exception("Unsupported spatial map: $map")
            }
        }

        graphic = VectorGraphic.triangles(Material(), triangles)
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.drawGraphic(graphic)

        when (selectorMode) {
            SelectorMode.QUAD -> renderer.drawQuadContour(SHAPE_SECONDARY, asQuadSelection(), lineThick)

            SelectorMode.AABBOX -> renderer.drawQuadContour(SHAPE_SECONDARY, asBoxSelection().toQuad(), lineThick)

            SelectorMode.EDGE -> renderer.drawSegment(SHAPE_SECONDARY, asEdgeSelection(), lineThick)

            SelectorMode.CIRCLE -> renderer.drawCircle(SHAPE_SECONDARY, asCircleSelection(), lineThick)
        }
    }
}

