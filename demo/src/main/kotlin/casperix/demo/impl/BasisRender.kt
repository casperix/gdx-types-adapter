package casperix.demo.impl

import casperix.demo.util.RenderConfig
import casperix.math.color.Colors.WHITE
import casperix.math.geometry.Line2f
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D



object BasisRender {
    private val BASIS_COLOR = WHITE.setAlpha(0.2f)

    fun render(renderer: Renderer2D) {
        val axisRange = 100f
        val pointStepRange = 10

        (-pointStepRange..pointStepRange).forEach { s ->
            renderer.drawPoint(BASIS_COLOR, Vector2f.X * s.toFloat(), RenderConfig.pointDiameter)
            renderer.drawPoint(BASIS_COLOR, Vector2f.Y * s.toFloat(), RenderConfig.pointDiameter)
        }

        renderer.drawLine(BASIS_COLOR, Line2f(Vector2f.X * -axisRange, Vector2f.X * axisRange), RenderConfig.lineThick)
        renderer.drawLine(BASIS_COLOR, Line2f(Vector2f.Y * -axisRange, Vector2f.Y * axisRange), RenderConfig.lineThick)

    }

}