package casperix.demo.impl.action


sealed interface GuiEntry

class SimpleAction(val name: String, val action: () -> Unit) : GuiEntry
class SwitchAction(val name: String, val action: (Boolean) -> Unit) : GuiEntry
class SelectorAction(val group:String, val name: String, val isDefault:Boolean = false, val action: (Boolean) -> Unit) : GuiEntry
class TitleEntry(val value: String) : GuiEntry
object DummyEntry : GuiEntry

