package casperix.demo.impl.action

class GuiManager {
    val entries = mutableListOf<GuiEntry>()


    operator fun plusAssign(action: GuiEntry) {
        entries += action
    }
}