package casperix.demo.impl.control_point

import casperix.math.vector.float32.Vector2f

enum class PointMode {
    LOCATION,
    DIRECTION,
}

class ControlPoint(var position: Vector2f = Vector2f.ZERO, val mode: PointMode = PointMode.LOCATION)