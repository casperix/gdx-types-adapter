package casperix.demo.impl.control_point

import casperix.demo.misc.CameraProvider
import casperix.demo.util.RenderConfig.CONTROL_POINT_BACK
import casperix.demo.util.RenderConfig.CONTROL_POINT_DEFAULT
import casperix.demo.util.RenderConfig.CONTROL_POINT_FOCUSED
import casperix.demo.util.RenderConfig.lineThick
import casperix.demo.util.RenderConfig.pointDiameter
import casperix.input.*
import casperix.math.color.Colors
import casperix.math.geometry.Line2f
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D
import casperix.renderer.material.Material


import casperix.signals.concrete.Signal

@ExperimentalUnsignedTypes
class ControlPointManager {
    val BACK_COLORS = listOf(Colors.RED, Colors.LIME, Colors.BLUE, Colors.YELLOW, Colors.CYAN, Colors.FUCHSIA, Colors.WHITE)

    var girdAlignStep: Float? = null
    var drawBack = true

    val groups = mutableListOf<List<ControlPoint>>()
    var focused: ControlPoint? = null
    var selected: ControlPoint? = null

    var pointDragged = Signal<ControlPoint>()


    operator fun plusAssign(points: List<ControlPoint>) {
        groups += points
    }

    fun input(event: InputEvent) {
        if (event is PointerEvent) {
            val cursor = screenToScene(event.position)

            if (event is PointerDown) {
                selected = getNearPoint(event.position)
            } else if (event is PointerMove) {
                selected?.also {
                    it.position = solvePosition(cursor)
                    pointDragged.set(it)
                }
            } else if (event is PointerUp) {
                selected = null
            } else {
                focused = getNearPoint(event.position)
            }

        }

    }

    private fun solvePosition(position: Vector2f): Vector2f {
        val girdAlignStep = girdAlignStep ?: return position
        return (position / girdAlignStep).round() * girdAlignStep
    }

    private fun getNearPoint(screenPosition: Vector2f): ControlPoint? {
        val camera = CameraProvider.camera
        val range = camera.zoom * 100f
        val cursor = screenToScene(screenPosition)
        val points = groups.flatten()
        return points
            .map { Pair(it, it.position.distTo(cursor)) }
            .filter { it.second < range }
            .minByOrNull { it.second }?.first
    }

    private fun screenToScene(value: Vector2f): Vector2f {
        val camera = CameraProvider.camera
        return camera.transform.unproject(value)
    }

    fun render(renderer: Renderer2D) {

        if (drawBack) {
            groups.forEach { group ->
                group.forEachIndexed { index, point ->
                    val a = group[index]
                    val b = group[(index + 1) % group.size]
                    renderer.drawLine(CONTROL_POINT_BACK, Line2f(a.position, b.position), lineThick * 8f)
                }
            }
        }

        groups.forEach { group ->
            group.forEachIndexed { index, point ->
                val backColor = BACK_COLORS[index % BACK_COLORS.size]

                val color = if (selected == point || focused == point) {
                    CONTROL_POINT_FOCUSED
                } else {
                    CONTROL_POINT_DEFAULT
                }
                renderer.drawCircle(
                    Material(backColor),
                    point.position,
                    pointDiameter * 0.75f,
                    pointDiameter * 1f
                )
                renderer.drawCircle(
                    Material(color),
                    point.position,
                    pointDiameter * 0f,
                    pointDiameter * 0.75f
                )

                if (point.mode == PointMode.DIRECTION) {
                    renderer.drawLine(CONTROL_POINT_BACK, Line2f(Vector2f.ZERO, point.position), lineThick * 8f)
                }
            }
        }
    }

}