package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.math.angle.float32.RadianFloat
import casperix.math.curve.float32.Arc2f
import casperix.math.curve.float32.Curve2f
import casperix.math.curve.float32.LineCurve2f
import casperix.math.geometry.CurveFactory
import casperix.math.geometry.Line2f
import casperix.math.geometry.PointAroundRay
import casperix.math.geometry.float32.Geometry2Float
import casperix.math.intersection.float32.Intersection2Float
import casperix.math.random.nextVector2f
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.rotateCCW
import casperix.math.vector.rotateCW
import kotlin.random.Random

class ArcPlusLineBuilder(random: Random) : CurveBuilder {
    val start = ControlPoint(random.nextVector2f(Vector2f(-1f), Vector2f(1f)))
    val startTangent = ControlPoint(random.nextVector2f(Vector2f(-1f), Vector2f(1f)))
    val finishTangent = ControlPoint(random.nextVector2f(Vector2f(-1f), Vector2f(1f)))
    val finish = ControlPoint(random.nextVector2f(Vector2f(-1f), Vector2f(1f)))


    override fun getPoints(): List<ControlPoint> {
        return listOf(start, startTangent, finishTangent, finish)
    }

    override fun build(): Curve2f {
        return CurveFactory.buildLineAndArc(
            start.position,
            startTangent.position,
            finishTangent.position,
            finish.position,
        )
    }

    fun buildLineAndArcTest(
        start: Vector2f,
        startAndTangent: Vector2f,
        finishAndTangent: Vector2f,
        finish: Vector2f,
        minLineLength: Float = 0.001f,
        maxArcRange: Float = 1000f
    ): Curve2f {
        val O = Intersection2Float.getLineWithLine(Line2f(start, startAndTangent), Line2f(finish, finishAndTangent))
            ?: return LineCurve2f(Line2f(start, finish))

        val rayOS = start - O
        val rayOF = finish - O

        val lengthOS = rayOS.length()
        val lengthOF = rayOF.length()

        if (lengthOS > maxArcRange || lengthOF > maxArcRange) {
            return LineCurve2f(Line2f(start, finish))
        }

        if (lengthOS > lengthOF) {
            return buildLineAndArcTest(finish, finishAndTangent, startAndTangent, start, minLineLength, maxArcRange).invert()
        }

        val pad1 = Geometry2Float.getPointAroundRay(start, finish, startAndTangent, 0f)
        val pad2 = Geometry2Float.getPointAroundRay(start, finish, finishAndTangent, 0f)
        val pad3 = Geometry2Float.getPointAroundRay(start, finish, O, 0f)
        if (pad1 != pad2  || pad1 != pad3) {
            return LineCurve2f(Line2f(start, finish))
        }

        val rayOA = rayOF.normalize() * lengthOS
        val A = O + rayOA

        val dirCS = rayOS.rotateCW()
        val dirCA = rayOA.rotateCCW()
        val center = Intersection2Float.getLineWithLine(Line2f.byDelta(start, dirCS), Line2f.byDelta(A, dirCA))
            ?: return LineCurve2f(Line2f(start, finish))

        val range = center.distTo(start)

        val middleAngle = RadianFloat.byDirection(O - center)
        var deltaAngle = RadianFloat.betweenAngle(middleAngle, RadianFloat.byDirection(start - center))


        val pad = Geometry2Float.getPointAroundRay(start, startAndTangent, finish, 0f)
        if (pad != PointAroundRay.LEFT) {
            deltaAngle = -deltaAngle
        }

        val arc = Arc2f(center, middleAngle - deltaAngle, deltaAngle * 2f, range)
        val line = LineCurve2f(A, finish)
        if (line.length() < minLineLength) {
            return arc
        }
        return arc + line

    }

}
