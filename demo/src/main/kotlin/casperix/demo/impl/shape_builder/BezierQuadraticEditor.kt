package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.control_point.ControlPointManager
import casperix.math.curve.float32.BezierQuadratic2f

class BezierQuadraticEditor(pointManager: ControlPointManager, initial: BezierQuadratic2f) : AbstractPointEditor(pointManager), ShapeEditor {
    val p0 = ControlPoint(initial.p0)
    val p1 = ControlPoint(initial.p1)
    val p2 = ControlPoint(initial.p2)

    init {
        selfPoints += listOf(p0, p1, p2)
    }

    override val shape: BezierQuadratic2f get() = BezierQuadratic2f(p0.position, p1.position, p2.position)

}