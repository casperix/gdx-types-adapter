package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.control_point.ControlPointManager
import casperix.math.curve.float32.Circle2f
import casperix.math.vector.float32.Vector2f

class CircleEditor(pointManager: ControlPointManager, initial: Circle2f) : AbstractPointEditor(pointManager), ShapeEditor {
    private val center = ControlPoint(initial.center)
    private val point = ControlPoint(initial.center + Vector2f.X * initial.range)


    init {
        selfPoints += listOf(center, point)
    }

    override val shape: Circle2f get() = Circle2f(center.position, center.position.distTo(point.position))
}