package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.math.curve.float32.LineCurve2f
import casperix.math.geometry.Line2f
import casperix.math.random.nextVector2f
import casperix.math.vector.float32.Vector2f
import kotlin.random.Random

class LineBuilder(random: Random) : CurveBuilder {
    private val start = ControlPoint(random.nextVector2f(Vector2f(-1f), Vector2f(1f)))
    private val finish = ControlPoint(random.nextVector2f(Vector2f(-1f), Vector2f(1f)))

    override fun getPoints(): List<ControlPoint> {
        return listOf(start, finish)
    }

    override fun build(): LineCurve2f {
        return LineCurve2f(Line2f(start.position, finish.position))
    }
}