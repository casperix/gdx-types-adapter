package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.control_point.ControlPointManager
import casperix.math.curve.float32.LineCurve2f
import casperix.math.geometry.Line2f
import casperix.math.straight_line.float32.LineSegment2f

class LineEditor(pointManager: ControlPointManager, initial: Line2f) : AbstractPointEditor(pointManager), ShapeEditor{
    val A = ControlPoint(initial.v0)
    val B = ControlPoint(initial.v1)

    val line: Line2f get() = Line2f(A.position, B.position)
    val segment:LineSegment2f get() = LineSegment2f(A.position, B.position)

    override val shape: LineCurve2f get() = LineCurve2f(line)

    init {
        selfPoints += listOf(A, B)
    }

}