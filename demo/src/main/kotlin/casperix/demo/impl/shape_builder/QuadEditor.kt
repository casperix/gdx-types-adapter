package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.control_point.ControlPointManager
import casperix.math.geometry.Quad2f

class QuadEditor(
    pointManager: ControlPointManager,
    initial: Quad2f,
) : AbstractPointEditor(pointManager), ShapeEditor {
    val A = ControlPoint(initial.v0)
    val B = ControlPoint(initial.v1)
    val C = ControlPoint(initial.v2)
    val D = ControlPoint(initial.v3)

    val quad: Quad2f get() = Quad2f(A.position, B.position, C.position, D.position)

    init {
        selfPoints += listOf(A, B, C, D)
    }

    override val shape: Any get() = quad

}