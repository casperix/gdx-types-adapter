package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.control_point.ControlPointManager
import casperix.math.curve.float32.LineCurve2f
import casperix.math.geometry.Line2f
import casperix.math.straight_line.float32.LineSegment2f

class SegmentEditor(pointManager: ControlPointManager, initial: LineSegment2f) : AbstractPointEditor(pointManager), ShapeEditor{
    val A = ControlPoint(initial.start)
    val B = ControlPoint(initial.finish)

    val line: LineSegment2f get() = LineSegment2f(A.position, B.position)

    override val shape get() =  line

    init {
        selfPoints += listOf(A, B)
    }

}