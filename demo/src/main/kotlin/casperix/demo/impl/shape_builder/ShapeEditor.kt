package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.control_point.ControlPointManager
import casperix.misc.Disposable

interface ShapeEditor : Disposable {
    fun getPoints(): List<ControlPoint>

    val shape: Any

    fun register(pointManager: ControlPointManager) {
        pointManager.groups += getPoints()
    }
}