package casperix.demo.misc

import casperix.gdx.renderer.impl.GdxTextureLoader
import casperix.renderer.material.Material
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Skin

@ExperimentalUnsignedTypes
object Assets {
    val skin = Skin(Gdx.files.internal("assets/uiskin.json"))
    val albedo = GdxTextureLoader.load("albedo.png")
    val normals = GdxTextureLoader.load("normals.png")

    val small_lorry = Material(
        null,
        GdxTextureLoader.load("assets/small_lorry_cargo_albedo.png"),
        GdxTextureLoader.load("assets/small_lorry_cargo_normals.png"),
    )


    val road = Material(
        null,
        GdxTextureLoader.load("assets/tiles2.png"),
        GdxTextureLoader.load("assets/tiles2_normals.png"),
    )


    val roadFlat = Material(
        GdxTextureLoader.load("assets/tiles2.png"),
    )


    val dummy = Material(
        null,
        GdxTextureLoader.load("assets/flat_albedo.png"),
        GdxTextureLoader.load("assets/flat_normals.png"),
    )
}