package casperix.demo.misc

import casperix.math.axis_aligned.int32.Dimension2i
import casperix.math.camera.OrthographicCamera2f
import casperix.math.vector.float32.Vector2f

object CameraProvider {
    val camera = OrthographicCamera2f(0.050f, Vector2f(2f), Dimension2i.ZERO)

}