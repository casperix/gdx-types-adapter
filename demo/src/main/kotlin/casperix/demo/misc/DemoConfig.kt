package casperix.demo.misc

object DemoConfig {
    val strangeScale = 0.01f
    val defaultZoom = 0.010f
}