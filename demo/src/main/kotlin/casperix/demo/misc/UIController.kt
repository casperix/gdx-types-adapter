package casperix.demo.misc

import casperix.demo.impl.AbstractDemo
import casperix.demo.util.UI.addRow
import casperix.demo.util.UI.createButton
import casperix.demo.util.UI.dummyHeight
import casperix.math.vector.float32.Vector2f
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.*

class UIController() {
    private val root = Table()
    private val left = Table()

    private val commonControl = Table()
    private val demoContent = Table()

    private val middle = Table()
    private val right = Table()
    private val skin = Assets.skin
    private val blackBackground = skin.get("default", TextField.TextFieldStyle::class.java).background

    val uiManager = GdxUIManager()
    val textTitle = Label("", skin)
    val fpsInfo = Label("", skin)
    val textInfo = Label("", skin)


    init {
        root.setFillParent(true)
        uiManager.addActor(root)

        left.background = blackBackground
        middle.background = blackBackground
        right.background = blackBackground

        root.add(left).width(300f).growY()
        root.add(middle).growX().expandY().top()
        root.add(right).width(300f).top()

        left.addRow(UISelector().root)

        middle.add(textTitle).row()

        right.addRow(fpsInfo)
        right.addRow(commonControl)

        commonControl.addRow(createButton("reset scene") { UIEvents.onRandomize.set() })
        commonControl.addRow(createButton("reset camera") { resetCamera() })
        commonControl.addRow(Actor()).height(dummyHeight)
        commonControl.addRow(textInfo)
        commonControl.addRow(Actor()).height(dummyHeight)

        right.addRow(demoContent)


        UIEvents.onDemo.then {
            demoContent.clear()
            if (it is AbstractDemo) {
                it.place(demoContent)
            }
        }

    }

    fun resetCamera() {
        val camera = CameraProvider.camera
        camera.zoom = DemoConfig.defaultZoom
        camera.position = Vector2f.ZERO
    }

    fun drawAndUpdate() {
        fpsInfo.setText("FPS: " + Gdx.graphics.framesPerSecond)
        uiManager.update()
    }
}