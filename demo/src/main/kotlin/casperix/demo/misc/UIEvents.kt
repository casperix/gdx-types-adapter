package casperix.demo.misc

import casperix.demo.impl.Demo
import casperix.demo.impl.DemoLauncher
import casperix.signals.concrete.EmptySignal
import casperix.signals.concrete.Signal

object UIEvents {
    val onRandomize = EmptySignal()
    val onDemoLauncher = Signal<DemoLauncher>()
    val onDemo = Signal<Demo>()
}