package casperix.demo.misc

import casperix.demo.DemoCollection
import casperix.demo.util.UI
import casperix.demo.util.UI.addRow
import casperix.gdx.ui.ToggleGroup
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.Table

class UISelector {
    private val selectorContent = Table()
    private val selectorScroll = ScrollPane(selectorContent)

    val root: Actor = selectorScroll

    init {
        val selectors = DemoCollection.items.map { demoEntry ->
            UI.createToggleButton(demoEntry.name) { UIEvents.onDemoLauncher.set(demoEntry) }
        }
        selectors.forEach { item ->
            selectorContent.addRow(item)
        }

        UIEvents.onDemoLauncher.then {
            val actual = selectors.firstOrNull { button ->
                (button.label.text.toString() == it.name)
            }
            actual?.isChecked = true
        }

        ToggleGroup(selectors, false)

    }
}