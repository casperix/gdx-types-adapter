package casperix.demo.util

import casperix.math.curve.float32.Curve2f

object CurveHelperExt {
    fun get(curve: Curve2f, steps: Int): List<Double> {
        val points = (0..steps).map {
            curve.getPosition(it.toFloat() / steps.toFloat()).toVector2d()
        }

        val dests = (0 until steps).map {
            val a = points[it]
            val b = points[it + 1]
            a.distTo(b)
        }

        return dests
    }

    fun getRough(curve: Curve2f, steps: Int): Double {
        val dests = get(curve, steps)
        val rough = dests.max() / dests.min()
        return rough
    }
}