package casperix.demo.util

import casperix.demo.misc.CameraProvider
import casperix.math.color.Colors

object RenderConfig {
    val SHAPE_MARKER = Colors.WHITE

    val SHAPE_PRIMARY = Colors.RED.setAlpha(0.7)
    val SHAPE_SECONDARY = Colors.BLUE.setAlpha(0.7)

    val CONTROL_POINT_BACK = Colors.BLACK.setAlpha(0.10f)
    val CONTROL_POINT_DEFAULT = Colors.GRAY.setAlpha(0.1f)
    val CONTROL_POINT_FOCUSED = Colors.WHITE.setAlpha(0.5f)

    val lineThick get() = CameraProvider.camera.zoom * 4f
    val pointDiameter get() = CameraProvider.camera.zoom * 12f

}