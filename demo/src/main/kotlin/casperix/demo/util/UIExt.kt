package casperix.demo.util

import casperix.demo.misc.Assets
import casperix.gdx.ui.addClickListener
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Cell
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton

object UI {
    val space = 2f
    val dummyHeight = 10f
    val skin = Assets.skin

    fun Table.addRow(actor: Actor): Cell<Actor> {
        return add(actor).space(space).growX().apply {
            row()
        }
    }


    fun createButton(label: String, click: () -> Unit): TextButton {
        return TextButton(label, skin).apply {
            addClickListener {
                click()
            }
        }
    }

    fun createToggleButton(label: String, click: (Boolean) -> Unit): TextButton {
        return TextButton(label, skin, "toggle").apply {
            addClickListener {
                click(isChecked)
            }
        }
    }


}
