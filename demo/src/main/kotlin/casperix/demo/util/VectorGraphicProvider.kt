package casperix.demo.util


//object VectorGraphicProvider {
//    val width = 30
//    val height = 30
//    val baseSize = 50
//    val baseScale = 0.5f
//
//    init {
//        val buffer = VertexBufferGrouped(width * height * 4, usePosition2 = true, useTangent = true)
//        (0 until width).forEach { ix ->
//            (0 until height).forEach { iy ->
//                var offset = (ix + iy * width) * 4
//                val x = -baseSize / 2 + (ix % baseSize)
//                val y = -baseSize / 2 + (iy % baseSize)
//                val s = baseScale
//                val step = 0.9f
//                val tangent = Vector2f.X
//
//                listOf(0f, 1f).forEach { ty ->
//                    listOf(0f, 1f).forEach { tx ->
//                        val sy = ty
//                        val sx = if (ty == 0f) tx else (1f - tx)
//
//                        buffer.setPosition2(offset, (x + sx * step) * s, (y + sy * step) * s)
//                        buffer.setTextureCoord(offset, sx, sy)
//                        buffer.setTangent(offset, tangent)
//                        offset++
//                    }
//                }
//
//            }
//        }
//
//    }
//
//    val texturedTangentGraphic = VectorGraphic(
//        Material(Colors.RED.toColor4f(), Assets.albedo, Assets.normals), VectorType.QUAD, ColTexTanArray(width * height * 4).apply {
//
//
//                }
//            }
//        }
//    )
//
//    val texturedGraphic = VectorGraphic(
//        Material(Colors.BLUE.toColor4f(), Assets.albedo, null), VectorType.QUAD, ColTexArray(width * height * 4).apply {
//
//            (0 until width).forEach { ix ->
//                (0 until height).forEach { iy ->
//                    val offset = (ix + iy * width) * 4
//                    val x = -baseSize / 2 + (ix % baseSize)
//                    val y = -baseSize / 2 + (iy % baseSize)
//                    val s = baseScale
//                    set(
//                        offset + 0,
//                        ColTexVertex(
//                            Vector2f((x + 0f) * s, (y + 0f) * s),
//                            Colors.WHITE.toFloatColor(),
//                            Vector2f(0f, 0f),
//                        )
//                    )
//                    set(
//                        offset + 1,
//                        ColTexVertex(
//                            Vector2f((x + 1f) * s, (y + 0f) * s),
//                            Colors.WHITE.toFloatColor(),
//                            Vector2f(1f, 0f),
//                        )
//                    )
//                    set(
//                        offset + 2,
//                        ColTexVertex(
//                            Vector2f((x + 1f) * s, (y + 1f) * s),
//                            Colors.WHITE.toFloatColor(),
//                            Vector2f(1f, 1f),
//                        )
//                    )
//                    set(
//                        offset + 3,
//                        ColTexVertex(
//                            Vector2f((x + 0f) * s, (y + 1f) * s),
//                            Colors.WHITE.toFloatColor(),
//                            Vector2f(0f, 1f),
//                        )
//                    )
//
//                }
//            }
//        }
//    )
//}