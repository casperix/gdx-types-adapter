package casperix.demo.util.naive_search

import casperix.math.vector.float32.Vector2f

class SpatialEntry2D<Value>(
    val position: Vector2f,
    val value: Value,
)