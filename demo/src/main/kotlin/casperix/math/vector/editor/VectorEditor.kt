package casperix.math.vector.editor

import casperix.demo.util.firstToEnd
import casperix.math.geometry.Quad
import casperix.math.geometry.Triangle
import casperix.math.interpolation.interpolateOf

object VectorEditor {

    /**
     * v1
     * |
     * |
     * m1--m2
     * |
     * |
     * v0------v2
     */
    fun divideTriangleToTriangleAndQuad(triangle:Triangle<PointData>): List<PolygonData> {
        return triangle.run {
            divideTriangleToTriangleAndQuad(v0, v1, v2)
        }
    }

    fun divideTriangleToTriangleAndQuad(v0: PointData, v1: PointData, v2: PointData): List<PolygonData> {
        val m1 = PointData((v0.position + v1.position) / 2f)
        val m2 = PointData((v1.position + v2.position) / 2f)

        return listOf(PolygonData(v0, v2, m2, m1), PolygonData(m1, m2, v1))
    }

    /**
     * v3---v2
     * |
     * m1---m2-->U
     * |
     * v0---v1
     */
    fun splitByUAxis(candidate: Quad<PointData>, factor: Float): List<PolygonData> {
        val m1 = PointData(interpolateOf(candidate.v0.position, candidate.v3.position, factor))
        val m2 = PointData(interpolateOf(candidate.v1.position, candidate.v2.position, factor))

        candidate.run {

            return listOf(
                PolygonData(
                    v0, v1,
                    m2, m1
                ),
                PolygonData(
                    m1, m2,
                    v2, v3
                ),
            )
        }
    }

    fun splitByVAxis(candidate: Quad<PointData>, factor: Float): List<PolygonData> {
        candidate.apply {
            return splitByUAxis(candidate.firstToEnd(), factor)
        }
    }
}