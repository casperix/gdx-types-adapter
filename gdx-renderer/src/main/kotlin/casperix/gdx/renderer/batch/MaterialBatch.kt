package casperix.gdx.renderer.batch

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.Vector3

interface MaterialBatch  {
    var lightPosition: Vector3
    var ambientColor: Color

    /**
     * vertex have 7 floats:
     * pos-x, pos-y, color, tex-u, tex-v, tangent-x, tangent-y
     */
    fun drawQuad(albedo: Texture, normal:Texture, quadVertices: FloatArray, offset: Int, count: Int)

    fun drawTriangle(albedo: Texture, normal:Texture, spriteVertices: FloatArray, offset: Int, count: Int)

    fun begin()
    fun end()
}