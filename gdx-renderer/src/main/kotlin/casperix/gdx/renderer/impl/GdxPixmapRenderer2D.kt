package casperix.gdx.renderer.impl

import casperix.gdx.graphics.toColor
import casperix.math.axis_aligned.float32.Box2f
import casperix.math.axis_aligned.int32.Box2i
import casperix.math.axis_aligned.int32.Dimension2i
import casperix.math.color.Color
import casperix.math.color.Colors.WHITE
import casperix.math.geometry.Quad2f
import casperix.math.geometry.Triangle2f
import casperix.math.quad_matrix.float32.Matrix3f
import casperix.math.vector.float32.Vector2f
import casperix.misc.Disposable
import casperix.renderer.Environment
import casperix.renderer.Renderer2D
import casperix.renderer.material.Material
import casperix.renderer.vector.VectorGraphic
import com.badlogic.gdx.graphics.Pixmap
import kotlin.math.roundToInt

@ExperimentalUnsignedTypes
class GdxPixmapRenderer2D(val pixmap: Pixmap) : Renderer2D, Disposable {
    override var environment = Environment()
    override var viewPort: Dimension2i = Dimension2i(pixmap.width, pixmap.height)
        set(value) {
            throw Exception("Unsupported")
        }

    init {
        if (viewPort.width != viewPort.height) {
            throw Exception("Non quad pixmap work uncorrected")
        }
    }
    override fun drawGraphic(graphic: VectorGraphic, modelTransform: Matrix3f) {
        TODO("Not yet implemented")
    }


    override fun clear() {
        pixmap.setColor(environment.backgroundColor.toColor())
        pixmap.fill()
    }

    override fun drawTriangle(material: Material, triangle: Triangle2f) {
        val transformedTriangle = triangle.convert { environment.viewMatrix.transform(it.expand(0f)).getXY() }

        material.color?.let {
            setColor(it)
        }


        pixmap.fillTriangle(
            transformedTriangle.v0.x.roundToInt(),
            transformedTriangle.v0.y.roundToInt(),
            transformedTriangle.v1.x.roundToInt(),
            transformedTriangle.v1.y.roundToInt(),
            transformedTriangle.v2.x.roundToInt(),
            transformedTriangle.v2.y.roundToInt()
        )

    }

    override fun flush() {

    }

    override fun getScissor(): Box2i? {
        TODO("Not yet implemented")
    }

    override fun setScissor(area: Box2i?) {
        TODO("Not yet implemented")
    }

    private fun setColor(color: Color?) {
        val gdxColor = (color ?: WHITE).toColor()
        pixmap.setColor(gdxColor)
    }

    override fun drawQuad(material: Material, quad: Quad2f) {
        val tQuad = quad.convert { environment.viewMatrix.transform(it.expand(0f)).getXY() }
        val t1 = tQuad.getFace(0)
        val t2 = tQuad.getFace(1)

        material.color?.let {
            setColor(it)
        }
        drawTriangle(material, t1)
        drawTriangle(material, t2)
    }

    override fun dispose() {

    }
}

