package casperix.gdx.renderer.impl

import casperix.misc.put
import casperix.misc.toUByteArray
import casperix.renderer.pixel_map.PixelMap
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Pixmap

@ExperimentalUnsignedTypes
object GdxTextureLoader {
    fun load(path: String): PixelMap {
        val pixmap = Pixmap(Gdx.files.internal(path))
        return when (pixmap.format) {
            Pixmap.Format.RGB888 -> PixelMap.RGB(
                pixmap.width,
                pixmap.height,
                pixmap.pixels.toUByteArray()
            )

            Pixmap.Format.RGBA8888 -> {

                PixelMap.RGBA(
                    pixmap.width,
                    pixmap.height,
                    pixmap.pixels.toUByteArray()
                )
            }

            else -> {
                throw Exception("Unsupported gdx format: ${pixmap.format}")
            }
        }
    }
}