package casperix.gdx.renderer.impl

object IndicesProvider {
    val indices = ShortArray(65536) { it.toShort() }
}