#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif
varying LOWP vec4 v_color;
varying vec2 v_tex_coords;
varying vec3 v_ts_light_pos;
varying vec3 v_ts_frag_pos;

uniform sampler2D u_texture_albedo;
uniform sampler2D u_texture_normal;

uniform vec3 u_ambient_color;

varying vec2 v_tangent;

void main()
{
    vec3 normal_color = texture(u_texture_normal, v_tex_coords).rgb;
    vec3 ts_normal = normalize(normal_color * 2.0 - 1.0);

    vec3 ts_light_dir = normalize(v_ts_light_pos - v_ts_frag_pos);

    float lightness = max(0.0, dot(ts_normal, ts_light_dir));

    vec4 albedo = texture(u_texture_albedo, v_tex_coords);
    gl_FragColor =  vec4(albedo.rgb * lightness + albedo.rgb * u_ambient_color, albedo.a) * v_color;
//    gl_FragColor =  vec4(albedo.rgb * lightness + albedo.rgb * u_ambient_color, albedo.a) * 0.01 + v_color;

    // specular
    //    vec3 viewDir = normalize(fs_in.TangentViewPos - fs_in.TangentFragPos);
    //    vec3 reflectDir = reflect(-lightDir, normal);
    //    vec3 halfwayDir = normalize(lightDir + viewDir);
    //    float spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
    //    vec3 specular = vec3(0.2) * spec;
}