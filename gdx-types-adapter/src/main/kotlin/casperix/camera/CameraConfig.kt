package casperix.camera

import casperix.math.axis_aligned.float32.Box2f

class CameraConfig(
    var minZoom: Float = 0.01f,
    val maxZoom: Float = 1f,
    val bound: Box2f? = null,
)