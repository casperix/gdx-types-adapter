package casperix.gdx.geometry

import casperix.math.array.int32.IntMap2D
import casperix.math.axis_aligned.float64.Box2d
import casperix.math.geometry.Line3d
import casperix.math.geometry.Quad
import casperix.math.geometry.delta
import casperix.math.geometry.float64.Plane3d
import casperix.math.quad_matrix.float32.Matrix3f
import casperix.math.quad_matrix.float32.Matrix4f
import casperix.math.quad_matrix.float64.Matrix4d
import casperix.math.quaternion.float64.QuaternionDouble
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.float32.Vector3f
import casperix.math.vector.float64.Vector2d
import casperix.math.vector.float64.Vector3d
import casperix.misc.toByteBuffer
import casperix.misc.toDirectByteBuffer
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.math.*
import com.badlogic.gdx.math.collision.Ray

fun Quad<Vector2d>.toPolygon(): Polygon {
    val p = Polygon()
    val vertices = mutableListOf<Float>()
    for (index in 0 until getVertexAmount()) {
        val vertex = getVertex(index)
        vertices.add(vertex.x.toFloat())
        vertices.add(vertex.y.toFloat())
    }
    return Polygon(vertices.toFloatArray())
}

fun Vector2d.toVector2(): Vector2 {
    return Vector2(x.toFloat(), y.toFloat())
}

fun Vector2.toVector2d(): Vector2d {
    return Vector2d(x.toDouble(), y.toDouble())
}

fun Vector2.toVector2f(): Vector2f {
    return Vector2f(x, y)
}

fun Vector3f.toVector3(): Vector3 {
    return Vector3(x, y, z)
}

fun Vector3d.toVector3(): Vector3 {
    return Vector3(x.toFloat(), y.toFloat(), z.toFloat())
}

fun Vector3.toVector3d(): Vector3d {
    return Vector3d(x.toDouble(), y.toDouble(), z.toDouble())
}

fun Vector3.toVector3f(): Vector3f {
    return Vector3f(x, y, z)
}


fun Quaternion.toQuaternion(): QuaternionDouble {
    return QuaternionDouble(x.toDouble(), y.toDouble(), z.toDouble(), w.toDouble())
}

fun QuaternionDouble.toQuaternion(): Quaternion {
    return Quaternion(x.toFloat(), y.toFloat(), z.toFloat(), w.toFloat())
}

fun Line3d.toRay(): Ray {
    return Ray(v0.toVector3(), delta().toVector3())
}

fun Ray.toLine3d(): Line3d {
    val origin = origin.toVector3d()
    return Line3d(origin, origin + direction.toVector3d())
}

fun Plane3d.toPlane(): Plane {
    return Plane(normal.toVector3(), destToOrigin.toFloat())
}

fun Plane.toPlane3d(): Plane3d {
    return Plane3d(normal.toVector3d(), d.toDouble())
}

fun Box2d.toRectangle(): Rectangle {
    return Rectangle(min.x.toFloat(), min.y.toFloat(), (max.x - min.x).toFloat(), (max.y - min.y).toFloat())
}

fun Matrix4.rotate(quaternion: QuaternionDouble) {
    this.rotate(quaternion.toQuaternion())
}

fun Matrix4.setToTranslation(value: Vector3d) {
    this.setToTranslation(value.x.toFloat(), value.y.toFloat(), value.z.toFloat())
}

fun Matrix4.translate(value: Vector3d) {
    this.translate(value.x.toFloat(), value.y.toFloat(), value.z.toFloat())
}

fun Matrix4.scale(value: Vector3d) {
    this.scale(value.x.toFloat(), value.y.toFloat(), value.z.toFloat())
}

fun Matrix4.scale(value: Vector3) {
    this.scale(value.x, value.y, value.z)
}

fun Matrix4.scale(value: Float) {
    this.scale(value, value, value)
}

fun Matrix4.scale(value: Double) {
    this.scale(value.toFloat(), value.toFloat(), value.toFloat())
}

fun Matrix4d.toMatrix4(): Matrix4 {
    return Matrix4(FloatArray(data.size) { data[it].toFloat() })
}

fun Matrix4f.toMatrix4(): Matrix4 {
    return Matrix4(data)
}

fun Matrix4.toMatrix4d(): Matrix4d {
    return Matrix4d(DoubleArray(`val`.size) { `val`[it].toDouble() })
}

fun Matrix4.toMatrix4f(): Matrix4f {
    return Matrix4f(`val`)
}

fun Matrix3f.toMatrix(): Matrix3 {
    return Matrix3(data.copyOf())
}

fun Matrix3f.toAffine2(): Affine2 {
    val a = Affine2()
    a.set(toMatrix())
    return a
}


fun IntMap2D.toPixmap(): Pixmap {
    val pixmap = Pixmap(sizeX, sizeY, Pixmap.Format.RGBA8888)
    pixmap.pixels.put(array.toDirectByteBuffer())
    return pixmap
}
