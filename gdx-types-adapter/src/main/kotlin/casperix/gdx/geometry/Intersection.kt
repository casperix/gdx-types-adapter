package casperix.gdx.geometry

import casperix.math.axis_aligned.float64.Box2d
import casperix.math.geometry.Line2d
import casperix.math.geometry.Line3d
import casperix.math.geometry.Triangle3d
import casperix.math.geometry.delta
import casperix.math.geometry.float64.Plane3d
import casperix.math.vector.float64.Vector2d
import casperix.math.vector.float64.Vector3d
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.math.collision.Ray

object Intersection {
    private val tmp = Vector3()

    fun distanceLinePoint(line: Line2d, point: Vector2d): Double {
        return Intersector.distanceSegmentPoint(line.v0.toVector2(), line.v1.toVector2(), point.toVector2()).toDouble()
    }

    fun lineWithPlane(line: Line3d, plane: Plane3d): Vector3d? {
        val result = Vector3()
        val value = Intersector.intersectLinePlane(
            line.v0.x.toFloat(), line.v0.y.toFloat(), line.v0.z.toFloat(),
            line.v1.x.toFloat(), line.v1.y.toFloat(), line.v1.z.toFloat(),
            plane.toPlane(),
            result
        )
        if (value >= 0.0) {
            return result.toVector3d()
        }
        return null
    }

    fun triangleWithTriangle(A: Triangle3d, B: Triangle3d): Boolean {
        if (lineWithTriangle(Line3d(A.v0, A.v1), B)) return true
        if (lineWithTriangle(Line3d(A.v1, A.v2), B)) return true
        if (lineWithTriangle(Line3d(A.v2, A.v0), B)) return true
        if (lineWithTriangle(Line3d(B.v0, B.v1), A)) return true
        if (lineWithTriangle(Line3d(B.v1, B.v2), A)) return true
        if (lineWithTriangle(Line3d(B.v2, B.v0), A)) return true
        return false
    }

    fun lineWithTriangle(line: Line3d, triangle: Triangle3d): Boolean {
        return Intersector.intersectRayTriangle(
            Ray(line.v0.toVector3(), line.delta().toVector3()),
            triangle.v0.toVector3(),
            triangle.v1.toVector3(),
            triangle.v2.toVector3(),
            tmp
        )
    }

    fun lineWithBox(ray: Line2d, box: Box2d): Boolean {
        return Intersector.intersectSegmentRectangle(
            ray.v0.x.toFloat(),
            ray.v0.y.toFloat(),
            ray.v1.x.toFloat(),
            ray.v1.y.toFloat(),
            box.toRectangle()
        )
    }

    fun lineWithLine(A: Line2d, B: Line2d): Vector2d? {
        val target = Vector2()
        if (!Intersector.intersectSegments(
                A.v0.x.toFloat(),
                A.v0.y.toFloat(),
                A.v1.x.toFloat(),
                A.v1.y.toFloat(),
                B.v0.x.toFloat(),
                B.v0.y.toFloat(),
                B.v1.x.toFloat(),
                B.v1.y.toFloat(),
                target
            )
        ) return null
        return target.toVector2d()
    }
}