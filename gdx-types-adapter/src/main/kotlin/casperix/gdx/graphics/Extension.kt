package casperix.gdx.graphics

import casperix.math.color.Color
import casperix.math.color.float32.Color4f

fun Color.toColor(): com.badlogic.gdx.graphics.Color {
    return toColor4f().toColor()
}


fun Color4f.toColor(): com.badlogic.gdx.graphics.Color {
    return com.badlogic.gdx.graphics.Color(red, green, blue, alpha)
}
