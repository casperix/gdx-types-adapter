package casperix.gdx.graphics

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture

fun Texture(internalPath:String, useMipMap:Boolean):Texture {
	return Texture(Gdx.files.internal(internalPath), useMipMap)
}