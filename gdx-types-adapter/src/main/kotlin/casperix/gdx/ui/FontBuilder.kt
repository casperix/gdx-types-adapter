package casperix.gdx.ui

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator

fun createFont(size: Int, fileName:String, characters: String = FreeTypeFontGenerator.DEFAULT_CHARS): BitmapFont {
	val fontGenerator = FreeTypeFontGenerator(Gdx.files.internal(fileName))
	return createFont(size, fontGenerator, characters)
}

fun createFont(size: Int, fontGenerator:FreeTypeFontGenerator, characters: String = FreeTypeFontGenerator.DEFAULT_CHARS): BitmapFont {
	val parameters = FreeTypeFontGenerator.FreeTypeFontParameter()
	parameters.size = size
	parameters.characters = characters
	return fontGenerator.generateFont(parameters)
}
